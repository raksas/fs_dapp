# Copyright (c) 2015 Davide Gessa
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

from libcontractvm import Wallet, ConsensusManager, DappManager

import time
import datetime

class ForumManager (DappManager.DappManager):
	def __init__ (self, consensusManager, wallet = None):
		super (ForumManager, self).__init__(consensusManager, wallet)

	def createPost (self, title, body):
		cid = self.produceTransaction ('forum.createPost', [title, body, self.wallet.getAddress()])
		return cid

	def commentPost (self, postID, comment):
		cid = self.produceTransaction ('forum.commentPost', [postID, comment, self.wallet.getAddress()])
		return cid

	def listPost (self):
		lista = self.consensusManager.jsonConsensusCall ('forum.listPost', [])['result']

		# if "lista" is empty
		if not lista:
			return "DB empty"
		else:
			# number of posts
			count = 1
			finalReturn = ""
	
			# for each post 
			for x in lista:
				finalReturn += "post " + str(count) + ": " + x + '\n'
				count += 1
			return finalReturn

	def getPostInfo (self, postID):
		lista = self.consensusManager.jsonConsensusCall ('forum.getPostInfo', [postID])['result']

		# if "lista" is empty
		if not lista:
			return "Post not in DB"
		else:
			finalReturn = "Title: " + lista['title'] + "\tBody: " + lista['body'] + '\n'

			# for each comment
			for x in lista['comments']:
				finalReturn += "user: " + x['userAddress'] + "\thas posted a comment: " + x['comment'] + "\twith message: " + x['commentID'] + '\n'
			return finalReturn

	def createPoll (self, title, choices, deadline):

		# checking if the date inserted is a correct date
		try :
			tempDeadline = time.mktime(datetime.datetime.strptime(deadline, "%d/%m/%Y").timetuple())
		except:
			return "Time not well formatted"
		
		cid = self.produceTransaction ('forum.createPoll', [title, choices, deadline, self.wallet.getAddress()])
		return  cid

	def listPolls (self):
		lista = self.consensusManager.jsonConsensusCall ('forum.listPolls', [])['result']

		# if "lista" is empty
		if not lista:
			return "DB empty"
		else:

			# number of polls
			count = 1
			finalReturn = ""

			# for each poll
			for x in lista:
				finalReturn += "poll " + str(count) + ": " + x + '\n'
				count += 1
			return finalReturn

	def vote (self, pollID, choice):
		cid = self.produceTransaction ('forum.vote', [pollID, choice, self.wallet.getAddress()])
		return cid

	def getPollInfo (self, pollID):
		lista = self.consensusManager.jsonConsensusCall ('forum.getPollInfo', [pollID])['result']

		# if "lista" is empty
		if not lista:
			return "Poll not in DB"
		else:
			finalReturn = "Title: " + lista['title'] + '\n' + "Deadline: " + lista['deadline']
			currTime = time.time()
			deadline = time.mktime(datetime.datetime.strptime(lista['deadline'], "%d/%m/%Y").timetuple())

			# checking if the poll is opne or not
			if currTime < deadline:
				finalReturn += "(open)\n"
			else:
				finalReturn += "(close)\n"

			tempReturn = ""

			# for each answer
			for x in lista['choices']:
				finalReturn += x['choice'] + ":\t" + str(len(x['votes'])) +'\n' 

				# for each vote
				for y in x['votes']:
					tempReturn += "user: " + y['userAddress'] + "\thas voted: " + x['choice'] + "\twith message: " + y['voteID'] +'\n'
			finalReturn += tempReturn
			return finalReturn

	def deletePoll (self, pollID):
		return self.produceTransaction ('forum.deletePoll', [pollID, self.wallet.getAddress()])

	def deletePost (self, postID):
		return self.produceTransaction ('forum.deletePost', [postID, self.wallet.getAddress()])

	def deleteComment (self, commentID):
		return self.produceTransaction ('forum.deleteComment', [commentID, self.wallet.getAddress()])

	def getUserInfo (self, userAddress):
		return self.consensusManager.jsonConsensusCall ('forum.getUserInfo', [userAddress])['result']

	def editPost (self, postID, title, body):
		return self.produceTransaction ('forum.editPost', [postID, title, body, self.wallet.getAddress()])

	def editComment (self, commentID, comment):
		return self.produceTransaction ('forum.editComment', [commentID, comment, self.wallet.getAddress()])
