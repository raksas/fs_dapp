# Copyright (c) 2015 Davide Gessa
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

import logging
import time
import datetime

from contractvmd import dapp, config, proto
from contractvmd.chain import message

logger = logging.getLogger(config.APP_NAME)


class ForumProto:
	DAPP_CODE = [ 0x01, 0x04 ]
	METHOD_POS = 0x01
	METHOD_COM = 0x02
	METHOD_POL = 0x03
	METHOD_VOT = 0x04
	METHOD_DEL_POL = 0x05
	METHOD_DEL_POS = 0x06
	METHOD_DEL_COM = 0x07
	METHOD_EDI_POS = 0x08
	METHOD_EDI_COM = 0x09
	METHOD_LIST = [METHOD_POS, METHOD_COM, METHOD_POL, METHOD_VOT, METHOD_DEL_POL, METHOD_DEL_POS, METHOD_DEL_COM, METHOD_EDI_POS, METHOD_EDI_COM]


class ForumMessage (message.Message):
	def createPost (title, body, userAddress):
		m = ForumMessage ()
		m.UserAddress = userAddress
		m.Title = title
		m.Body = body
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_POS
		return m

	def commentPost (postID, comment, userAddress):
		m = ForumMessage ()
		m.PostID = postID
		m.Comment = comment
		m.UserAddress = userAddress
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_COM
		return m

	def createPoll (title, choices, deadline, userAddress):
		m = ForumMessage ()
		m.UserAddress = userAddress
		m.Title = title
		m.Choices = choices
		m.Deadline = deadline
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_POL
		return m

	def vote (pollID, choice, userAddress):
		m = ForumMessage ()
		m.PollID = pollID
		m.Choice = choice
		m.UserAddress = userAddress
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_VOT
		return m

	def deletePoll (pollID, userAddress):
		m = ForumMessage ()
		m.PollID = pollID
		m.UserAddress = userAddress
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_DEL_POL
		return m

	def deletePost (postID, userAddress):
		m = ForumMessage ()
		m.PostID = postID
		m.UserAddress = userAddress
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_DEL_POS
		return m

	def deleteComment (commentID, userAddress):
		m = ForumMessage ()
		m.CommentID = commentID
		m.UserAddress = userAddress
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_DEL_COM
		return m

	def editPost (postID, title, body, userAddress):
		m = ForumMessage ()
		m.PostID = postID
		m.Title = title
		m.Body = body
		m.UserAddress = userAddress
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_EDI_POS
		return m

	def editComment (commentID, comment, userAddress):
		m = ForumMessage ()
		m.CommentID = commentID
		m.Comment = comment
		m.UserAddress = userAddress
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_EDI_COM
		return m

	def toJSON (self):
		data = super (ForumMessage, self).toJSON ()

		if self.Method == ForumProto.METHOD_POS:
			data['title'] = self.Title
			data['body'] = self.Body
			data['userAddress'] = self.UserAddress

		elif self.Method == ForumProto.METHOD_COM:
			data['postID'] = self.PostID
			data['comment'] = self.Comment	
			data['userAddress'] = self.UserAddress

		elif self.Method == ForumProto.METHOD_POL:
			data['title'] = self.Title
			data['choices'] = self.Choices
			data['deadline'] = self.Deadline
			data['userAddress'] = self.UserAddress

		elif self.Method == ForumProto.METHOD_VOT:
			data['pollID'] = self.PollID
			data['choice'] = self.Choice
			data['userAddress'] = self.UserAddress	

		elif self.Method == ForumProto.METHOD_DEL_POL:
			data['pollID'] = self.PollID
			data['userAddress'] = self.UserAddress	

		elif self.Method == ForumProto.METHOD_DEL_POS:
			data['postID'] = self.PostID
			data['userAddress'] = self.UserAddress	

		elif self.Method == ForumProto.METHOD_DEL_COM:
			data['commentID'] = self.CommentID
			data['userAddress'] = self.UserAddress	

		elif self.Method == ForumProto.METHOD_EDI_POS:
			data['postID'] = self.PostID
			data['title'] = self.Title
			data['body'] = self.Body
			data['userAddress'] = self.UserAddress	

		elif self.Method == ForumProto.METHOD_EDI_COM:
			data['commentID'] = self.CommentID
			data['comment'] = self.Comment
			data['userAddress'] = self.UserAddress	

		else:
			return None

		return data


class ForumCore (dapp.Core):
	def __init__ (self, chain, database):
		database.init ('posts', [])
		database.init ('polls', [])
		super (ForumCore, self).__init__ (chain, database)

	def createPost (self, postID, title, body, userAddress):
		self.database.listappend('posts',postID)
		self.database.init(postID, {'postID': postID, 'title': title, 'body': body, 'comments': [], 'userAddress': userAddress})

	def commentPost (self, postID, commentID, comment, userAddress):
		# getting post info by its postID
		postInfo = ForumCore.getPostInfo(self, postID)

		# if the post has been found
		if postInfo:

			# add the comment at the post
			postInfo['comments'].append({'commentID': commentID, 'comment': comment, 'userAddress': userAddress})

			# overwrite the post with new information
			self.database.set (postID, postInfo)

	def listPost (self):
		# return postsID
		return self.database.get ('posts')

	def getPostInfo (self, postID):
		allPostID = self.database.get ('posts')

		# if the ID passed in input is indeed a postID (checks if the postID is in the list of all the postsID)
		if postID in allPostID:
			return self.database.get(postID)
		else:
			return []

	def createPoll (self, pollID, title, choices, deadline, userAddress):
		self.database.listappend('polls', pollID)

		# this list contains votes' information and all the answer
		tempChoices = []

		# for each answer in the list "choices" set a dictionary
		for x in choices:

			# the dictionary has a field "choice"(representing an answer) and a list "votes"(now empty)
			tempChoices.append({'choice': x, 'votes': []})
		self.database.init (pollID, {'pollID': pollID, 'title': title, 'choices': tempChoices, 'deadline': deadline, 'userAddress': userAddress})

	def listPolls (self):
		# return pollsID
		return self.database.get ('polls')

	def vote (self, pollID, voteID, userAddress, choice):
		
		# flag that says if an user has already voted the post
		alreadyVoted = False

		# flag to check if the choice selected by the user exists or not
		choiceFound = False

		# getting poll info by its postID
		pollInfo = ForumCore.getPollInfo(self, pollID)

		# if the ID passed in input is indeed a pollID
		if pollInfo:

			# getting time
			currTime = time.time()

			# casting the deadline(string) to unicode timestamp
			deadline = time.mktime(datetime.datetime.strptime(pollInfo['deadline'], "%d/%m/%Y").timetuple())

			# checking if the poll is open
			if currTime < deadline:

				# for each answer
				for y in pollInfo['choices']:

					# for each vote
					for x in y['votes']:
						
						# check if the userAddress if this vote is equal to the userAddress is voting
						if x['userAddress'] == userAddress:
							alreadyVoted = True
							break

					# if the answer choosen exists in the list
					if y['choice'] == choice: 

						# insert vote's information
						y['votes'].append({'userAddress': userAddress, 'voteID': voteID})
						choiceFound = True

				# if the user has never voted the poll before now, and the answer is indeed a correct choice
				if (alreadyVoted == False) and (choiceFound == True):

					# overwrite the poll with new information
					self.database.set(pollID, pollInfo)
	
	def getPollInfo (self, pollID):
		allPollID = self.database.get ('polls')

		# if the ID passed in input is indeed a pollID (checks if the pollID is in the list of all the pollsID)
		if pollID in allPollID:
			return self.database.get(pollID)
		else:
			return []

	def deletePoll (self, pollID, userAddress):
		pollInfo = ForumCore.getPollInfo(self, pollID)
		if pollInfo:
	
			# check if the userAddress is the owner of the poll
			if pollInfo['userAddress'] == userAddress:
				self.database.listremove('polls',pollID)
				self.database.delete(pollID)

	def deletePost (self, postID, userAddress):
		postInfo = ForumCore.getPostInfo(self, postID)
		if postInfo:

			# check if the userAddress is the owner of the post
			if postInfo['userAddress'] == userAddress:
				self.database.listremove('posts',postID)
				self.database.delete(postID)

	def deleteComment (self, commentID, userAddress):
		allPostID = ForumCore.listPost(self)

		# for each poll
		for x in allPostID:
			postInfo = self.database.get(x)

			# for each comment
			for y in postInfo['comments']:

				# if the commentID is the same of the one passed in input
				if y['commentID'] == commentID:

					# check if the userAddress is the same of the one passed in input
					if y['userAddress'] == userAddress:

						# remove the comment and overwrite the post with new information
						postInfo['comments'].remove(y)
						self.database.set(postInfo['postID'],postInfo)

	def getUserInfo (self, userAddress):
		postReturn = "Posts: \n"
		pollReturn = "Polls: \n"
		commentReturn = "Comments: \n"

		allPostID = self.database.get ('posts')
		allPollID = self.database.get ('polls')

		# for each post
		for x in allPostID:
			postInfo = self.database.get(x)

			# check the owner
			if postInfo['userAddress'] == userAddress:
				postReturn += postInfo['postID'] +'\n'

			# for each comment
			for y in postInfo['comments']:

				# check the owner
				if y['userAddress'] == userAddress:
					commentReturn += "comID :" + y['commentID'] + "\t in postID: " + postInfo['postID'] + '\n'

		# for each poll
		for x in allPollID:
			pollInfo = self.database.get(x)

			# check the owner
			if pollInfo['userAddress'] == userAddress:
				pollReturn += pollInfo['pollID'] +'\n'

		return postReturn + pollReturn + commentReturn

	def editPost (self, postID, title, body, userAddress):
		postInfo = ForumCore.getPostInfo(self, postID)

		# check the owner
		if postInfo and postInfo['userAddress'] == userAddress:

			# remove the post from db
			ForumCore.deletePost(self, postID, userAddress)

			# create the same post with old comment and new title and body 
			self.database.listappend('posts',postID)
			self.database.init(postID, {'postID': postID, 'title': title, 'body': body, 'comments': postInfo['comments'], 'userAddress': userAddress})

	def editComment (self, commentID, comment, userAddress):
		allPostID = ForumCore.listPost(self)

		# for each post
		for x in allPostID:
			postInfo = self.database.get(x)

			# for each comment
			for y in postInfo['comments']:

				# check commentID
				if y['commentID'] == commentID:

					#check owner
					if y['userAddress'] == userAddress:

						# remove the comment
						postInfo['comments'].remove(y)

						# create the same post with old title and body but with new comment
						postInfo['comments'].append({'commentID': commentID, 'comment': comment, 'userAddress': userAddress})
						self.database.set(postInfo['postID'],postInfo)

class ForumAPI (dapp.API):
	def __init__ (self, core, dht, api):
		self.api = api
		rpcmethods = {}

		rpcmethods["listPost"] = {
			"call": self.method_listPost,
			"help": {"args": [], "return": {}}
		}

		rpcmethods["getPostInfo"] = {
			"call": self.method_getPostInfo,
			"help": {"args": ["postID"], "return": {}}
		}

		rpcmethods["createPost"] = {
			"call": self.method_createPost,
			"help": {"args": ["title", "body", "userAddress"], "return": {}}
		}

		rpcmethods["commentPost"] = {
			"call": self.method_commentPost,
			"help": {"args": ["postID", "comment", "userAddress"], "return": {}}
		}

		rpcmethods["createPoll"] = {
			"call": self.method_createPoll,
			"help": {"args": ["title", "choices", "deadline", "userAddress"], "return": {}}
		}

		rpcmethods["listPolls"] = {
			"call": self.method_listPolls,
			"help": {"args": [], "return": {}}
		}

		rpcmethods["vote"] = {
			"call": self.method_vote,
			"help": {"args": ["pollID", "choice"], "return": {}}
		}

		rpcmethods["getPollInfo"] = {
			"call": self.method_getPollInfo,
			"help": {"args": ["pollID"], "return": {}}
		}

		rpcmethods["deletePoll"] = {
			"call": self.method_deletePoll,
			"help": {"args": ["pollID", "userAddress"], "return": {}}
		}

		rpcmethods["deletePost"] = {
			"call": self.method_deletePost,
			"help": {"args": ["postID", "userAddress"], "return": {}}
		}

		rpcmethods["deleteComment"] = {
			"call": self.method_deleteComment,
			"help": {"args": ["commentID", "userAddress"], "return": {}}
		}

		rpcmethods["getUserInfo"] = {
			"call": self.method_getUserInfo,
			"help": {"args": ["userAddress"], "return": {}}
		}

		rpcmethods["editPost"] = {
			"call": self.method_editPost,
			"help": {"args": ["postID", "title", "body", "userAddress"], "return": {}}
		}

		rpcmethods["editComment"] = {
			"call": self.method_editComment,
			"help": {"args": ["commentID", "comment", "userAddress"], "return": {}}
		}


		errors = { }

		super (ForumAPI, self).__init__(core, dht, rpcmethods, errors)


	def method_listPost (self):
		return self.core.listPost ()

	def method_getPostInfo (self, postID):
		return self.core.getPostInfo (postID)

	def method_createPost (self, title, body, userAddress):
		msg = ForumMessage.createPost (title, body, userAddress)
		return self.createTransactionResponse (msg)

	def method_commentPost (self, postID, comment, userAddress):
		msg = ForumMessage.commentPost (postID, comment, userAddress)
		return self.createTransactionResponse (msg)

	def method_createPoll (self, title, choices, deadline, userAddress):	
		msg = ForumMessage.createPoll (title, choices, deadline, userAddress)
		return self.createTransactionResponse (msg)

	def method_listPolls (self):
		return self.core.listPolls ()

	def method_vote (self, postID, choice, userAddress):
		msg = ForumMessage.vote (postID, choice, userAddress)
		return self.createTransactionResponse (msg)

	def method_getPollInfo (self, pollID):
		return self.core.getPollInfo (pollID)

	def method_deletePoll (self, pollID, userAddress):
		msg = ForumMessage.deletePoll (pollID, userAddress)
		return self.createTransactionResponse (msg)

	def method_deletePost (self, postID, userAddress):
		msg = ForumMessage.deletePost (postID, userAddress)
		return self.createTransactionResponse (msg)

	def method_deleteComment (self, commentID, userAddress):
		msg = ForumMessage.deleteComment (commentID, userAddress)
		return self.createTransactionResponse (msg)

	def method_getUserInfo (self, userAddress):
		return self.core.getUserInfo (userAddress)

	def method_editPost (self, postID, title, body, userAddress):
		msg = ForumMessage.editPost (postID, title, body, userAddress)
		return self.createTransactionResponse (msg)

	def method_editComment (self, commentID, comment, userAddress):
		msg = ForumMessage.editComment (commentID, comment, userAddress)
		return self.createTransactionResponse (msg)

class forum (dapp.Dapp):
	def __init__ (self, chain, db, dht, apiMaster):
		self.core = ForumCore (chain, db)
		apiprov = ForumAPI (self.core, dht, apiMaster)
		super (forum, self).__init__(ForumProto.DAPP_CODE, ForumProto.METHOD_LIST, chain, db, dht, apiprov)

	def handleMessage (self, m):
		if m.Method == ForumProto.METHOD_POS:
			logger.pluginfo ('Found CREATE POST of %s', m.Data['title'])
			self.core.createPost (m.Hash, m.Data['title'], m.Data['body'], m.Data['userAddress'])

		elif m.Method == ForumProto.METHOD_COM:
			logger.pluginfo ('Found COMMENT POST of %s', m.Data['comment'])
			self.core.commentPost (m.Data['postID'], m.Hash, m.Data['comment'], m.Data['userAddress'])

		elif m.Method == ForumProto.METHOD_POL:
			logger.pluginfo ('Found CREATE POLL of %s', m.Data['title'])
			self.core.createPoll (m.Hash, m.Data['title'], m.Data['choices'], m.Data['deadline'], m.Data['userAddress'])

		elif m.Method == ForumProto.METHOD_VOT:
			logger.pluginfo ('Found VOTE POLL of %s', m.Data['choice'])
			self.core.vote (m.Data['pollID'], m.Hash, m.Data['userAddress'], m.Data['choice'])

		elif m.Method == ForumProto.METHOD_DEL_POL:
			logger.pluginfo ('Found DELETE POLL of %s', m.Data['pollID'])
			self.core.deletePoll (m.Data['pollID'], m.Data['userAddress'])

		elif m.Method == ForumProto.METHOD_DEL_POS:
			logger.pluginfo ('Found DELETE POST of %s', m.Data['postID'])
			self.core.deletePost (m.Data['postID'], m.Data['userAddress'])

		elif m.Method == ForumProto.METHOD_DEL_COM:
			logger.pluginfo ('Found DELETE COMMENT of %s', m.Data['commentID'])
			self.core.deleteComment (m.Data['commentID'], m.Data['userAddress'])

		elif m.Method == ForumProto.METHOD_EDI_POS:
			logger.pluginfo ('Found EDIT POST of %s', m.Data['postID'])
			self.core.editPost (m.Data['postID'], m.Data['title'], m.Data['body'], m.Data['userAddress'])

		elif m.Method == ForumProto.METHOD_EDI_COM:
			logger.pluginfo ('Found EDIT COMMENT of %s', m.Data['commentID'])
			self.core.editComment (m.Data['commentID'], m.Data['comment'], m.Data['userAddress'])
