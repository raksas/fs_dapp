#!/usr/bin/python3
# Copyright (c) 2015 Davide Gessa
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

from libcontractvm import Wallet, WalletExplorer, ConsensusManager
from forum import ForumManager
import sys
import time
import os

consMan = ConsensusManager.ConsensusManager ()
consMan.bootstrap ("http://127.0.0.1:8181")

walletAlice = WalletExplorer.WalletExplorer (wallet_file='Alice.wallet')
walletBob = WalletExplorer.WalletExplorer (wallet_file='Bob.wallet')

Alice = ForumManager.ForumManager (consMan, wallet=walletAlice)
Bob = ForumManager.ForumManager (consMan, wallet=walletBob)


print('****** BASIC ******')




postid = Alice.createPost ('Hello post', 'Post di test')
print("POST -> ",postid)

consMan.waitBlock()
time.sleep (5)

print(Alice.listPost())                                                                                     
commid = Alice.commentPost (postid, 'This is a comment')
print("COMMENT -> ",commid)

consMan.waitBlock()
time.sleep (5)

print(Alice.getPostInfo (postid))                                                               
postid2 = Bob.createPost ('Hello post 2', 'Post di test2')
print("POST -> ",postid2)
commid2 = Bob.commentPost (postid, 'This is a comment of B')
print("COMMENT -> ",commid2)

consMan.waitBlock()
time.sleep (5)

print(Bob.getPostInfo (postid))     




print('****** ADVANCED ******')




pollid = Alice.createPoll ('Title', ['answer1', 'answer2', 'answer3'], '10/10/2016')
print("POLL -> ",pollid)

consMan.waitBlock()
time.sleep (5)

print(Alice.listPolls ())                                                                              
voteid1 = Alice.vote (pollid, 'answer1')
print("VOTE -> ",voteid1)
voteid2 = Alice.vote (pollid, 'answer2')  
print("VOTE -> ",voteid2)

consMan.waitBlock()
time.sleep (5)
                                                        
print(Alice.getPollInfo (pollid))                                                                        
voteid3 = Bob.vote (pollid, 'answer2')
print("VOTE -> ",voteid3)

consMan.waitBlock()
time.sleep (5)

print(Alice.getPollInfo (pollid))                                                                         
pollid2 = Bob.createPoll ('Title', ['answer1', 'answer2', 'answer3'], '05/08/2017')
print("POLL -> ",pollid2)

consMan.waitBlock()
time.sleep (5)

print(Alice.listPolls ())                                                                                 




print('****** EXPERT ******')




print(Alice.getUserInfo (walletAlice.getAddress()))
Bob.editComment (commid, 'New comment 1 message')                         
Alice.editComment (commid, 'New comment 2 message')
Bob.editPost (postid, 'New hello post', 'New message!')                
Alice.editPost (postid, 'New hello post A', 'New message!')

consMan.waitBlock()
time.sleep (5)

print(Alice.listPost ())                                                                         
Alice.deleteComment (commid)
commid3 = Bob.commentPost (postid, 'This is a new comment')

consMan.waitBlock()
time.sleep (5)
 
print(Alice.getPostInfo (postid))                                                        
Alice.deleteComment (commid3)                                                        
Bob.deleteComment (commid3)

consMan.waitBlock()
time.sleep (5)

print(Alice.getPostInfo (postid))                                                         
Bob.deletePost (postid)                                                        
Alice.deletePost (postid)

consMan.waitBlock()
time.sleep (5)

print(Alice.listPost ())                                                                         
Bob.deletePoll (pollid2)

consMan.waitBlock()
time.sleep (5)

print(Alice.listPolls ())                                                              
print(Alice.getUserInfo (walletBob.getAddress()))
print(Bob.getUserInfo (walletBob.getAddress()))


