#!/usr/bin/python3
# Copyright (c) 2015 Davide Gessa
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.
from libcontractvm import Wallet, WalletExplorer, ConsensusManager
from forum import ForumManager
import sys
import time
import os

consMan = ConsensusManager.ConsensusManager ()
consMan.bootstrap ("http://127.0.0.1:8181")

wallet = WalletExplorer.WalletExplorer (wallet_file='Alice.wallet')
srMan = ForumManager.ForumManager (consMan, wallet=wallet)

os.system ('clear')

title = input ('Insert the title: ')
choices = []

tempChoice = input ('insert a choice: ')
choices.append(tempChoice)

while (input ('another one? (y/n): ') == 'y'):
	tempChoice = input ('insert a choice: ')
	choices.append(tempChoice)


deadline = input ('Insert the deadline: ')


print (srMan.createPoll (title, choices, deadline))

	
